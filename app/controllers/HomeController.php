<?php

use Illuminate\Support\Facades\Validator;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function flight(){
	  $rules = array(

			'start_date'    => 'required|date_format:d/m/Y|after:tomorrow',
			'return_date' => 'required|date_format:d/m/Y|after:start_date',
			'adult_passengers' => 'required|numeric|min:1',
			'origin' => 'required|string',
			'destination' => 'required|string',
			'max_stops' => 'required',
			'max_price' => 'required|numeric|min:1',
			'max_price_currency' => 'required|string:ISO-4217'
		);
		/*$validator = Validator::make(
			array('start_date' => Request::get('start_date'),
				'return_date' =>Request::get('return_date'),
				'adult_passengers' =>Request::get('adult_passengers'),
				'origin' =>Request::get('origin'),
				'destination' =>Request::get('destination'),
				'max_stops' =>Request::get('max_stops'),
				'max_price' =>Request::get('max_price'),
				'max_price_currency' =>Request::get('max_price_currency')
				  )
		);*/
		$validator = Validator::make($data = Input::all(),$rules);

		if ($validator->fails())
		{
			return $validator->messages()->toJson();
		}



//////post data to qpxexpress that comes from get data ...
		$data = array ( "request" => array(
			"passengers" => array(
		   "slice" => array(
				array(
					'origin' => Request::get('origin'),
					'destination' => Request::get('destination'),
					'date' => Request::get('return_date')
			)
		)
		)));
		$data_string = json_encode($data);
		$ch = curl_init('https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyAvo3-DW2ZPQfBZTzipoKEsCe6sOehhk4M');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		$result = curl_exec($ch);
		curl_close($ch);

		/* then I echo the result for testing purposes */

		//echo  json_encode($result);



//============ This for print request as json ===========================
				  return Response::json(array(
						'error' => false,
						'urls' => $data ),
					200
				);
		//============ This for print request as json ===========================

	}

}
